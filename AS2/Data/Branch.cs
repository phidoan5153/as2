﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AS2.Data
{
	[Table("Branch")]
	public class Branch
	{
		[Key]
		public int BranchId { get; set; }
		[MaxLength(100)]
		public string Name { get; set; }
		[MaxLength(100)]
		public string Address { get; set; }
		[MaxLength(100)]
		public string City { get; set; }
		[MaxLength(100)]
		public string State { get; set; }
		[MaxLength(100)]
		public string ZipCode { get; set; }
	}
}
