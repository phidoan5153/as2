﻿using Microsoft.EntityFrameworkCore;

namespace AS2.Data
{
	public class BranchContext : DbContext
	{
		public BranchContext(DbContextOptions<BranchContext> opt): base(opt) { }
		#region DBSet
		public DbSet<Branch>? Branches { get; set; }
		#endregion
	}
}
