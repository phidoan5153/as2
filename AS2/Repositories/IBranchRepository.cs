﻿using AS2.Data;
using AS2.Models;

namespace AS2.Repositories
{
	public interface IBranchRepository
	{
		public Task<List<BranchModel>> GetAll();
		public Task<BranchModel> Get(int id); 
		public Task<int> Add(BranchModel branch);	
		public Task Update(int id, BranchModel branch);
		public Task Delete(int id);
	}
}
