﻿using AS2.Data;
using AS2.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AS2.Repositories
{
	public class BranchRepository : IBranchRepository
	{
		private readonly BranchContext _context;
		private readonly IMapper _mapper;

		public BranchRepository(BranchContext context, IMapper mapper) {
			_context = context;
			_mapper = mapper;
		}

		public async Task<int> Add(BranchModel branch)
		{
			var newBranch = _mapper.Map<Branch>(branch);
			_context.Branches!.Add(newBranch);
			await _context.SaveChangesAsync();
			return newBranch.BranchId;
		}

		public async Task Delete(int id)
		{
			var deleteBranch = _context.Branches!.SingleOrDefault(b => b.BranchId == id);
			if (deleteBranch != null)
			{
				_context.Branches!.Remove(deleteBranch);
				await _context.SaveChangesAsync();
			}
		}

		public async Task<BranchModel> Get(int id)
		{
			var branch = await _context.Branches!.FindAsync(id);
			return _mapper.Map<BranchModel>(branch);
		}

		public async Task<List<BranchModel>> GetAll()
		{
			var branches = await _context.Branches!.ToListAsync();
			return _mapper.Map<List<BranchModel>>(branches);
		}

		public async Task Update(int id, BranchModel branch)
		{
			if (id == branch.BranchId)
			{
				var updateBranch = _mapper.Map<Branch>(branch);
				_context.Branches!.Update(updateBranch);
				await _context.SaveChangesAsync();
			}
		}
	}
}
