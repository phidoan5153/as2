﻿using System.ComponentModel.DataAnnotations;

namespace AS2.Models
{
	public class BranchModel
	{
		public int BranchId { get; set; }
		[MaxLength(100)]
		public string Name { get; set; }
		[MaxLength(100)]
		public string Address { get; set; }
		[MaxLength(100)]
		public string City { get; set; }
		[MaxLength(100)]
		public string State { get; set; }
		[MaxLength(100)]
		public string ZipCode { get; set; }
	}
}
