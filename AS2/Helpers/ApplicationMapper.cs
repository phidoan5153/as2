﻿using AS2.Data;
using AS2.Models;
using AutoMapper;

namespace AS2.Helpers
{
	public class ApplicationMapper : Profile
	{
		public ApplicationMapper() {
			CreateMap<Branch, BranchModel>().ReverseMap(); 
		}
	}
}
