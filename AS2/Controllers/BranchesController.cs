﻿using AS2.Models;
using AS2.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AS2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BranchesController : ControllerBase
	{
		private readonly IBranchRepository _branchRepo;

		public BranchesController(IBranchRepository repository) { 
			_branchRepo = repository;
		}
		[HttpGet]
		public async Task<IActionResult> GetAll()
		{
			try
			{
				return Ok(await _branchRepo.GetAll());
			} catch (Exception ex)
			{
				return BadRequest();
			}
		}
		[HttpGet("{id}")]
		public async Task<IActionResult> GetById(int id)
		{
			var branch = await _branchRepo.Get(id);
			return branch == null ? NotFound() : Ok(branch);
		}
		[HttpPost]
		public async Task<IActionResult> AddNewBranch(BranchModel branch)
		{
			try
			{
				var newBranch = await _branchRepo.Add(branch);
				var branchId = await _branchRepo.Get(newBranch);
				return branchId == null ? NotFound() : Ok(branchId);
			} catch (Exception ex)
			{
				return BadRequest();
			}
		}
		[HttpPut("{id}")]
		public async Task<IActionResult> UpdateBook(int id, BranchModel branch)
		{
			await _branchRepo.Update(id, branch);
			return Ok();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteBranch(int id)
		{
			await _branchRepo.Delete(id);
			return Ok();
		}
	}
}
